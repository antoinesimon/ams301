#include "headers.hpp"

#include <cmath>

int myRank;
int nbTasks;

ScaVector maskForResidual;

// Computes partial scalar products and then reduces the result in order to properly compute the residuals and errors
// It also takes into account the mask that prevent any node from being included several times in the computation
double prod_scal1(ScaVector& u, ScaVector& v, ScaVector& maskForResidual){
    double local = 0;
    double global = 0;

    for(int i =0; i < u.size(); i++){
      local += u(i)*v(i)*maskForResidual(i);
    }

    MPI_Allreduce(&local, &global, 1, MPI_DOUBLE, MPI_SUM, MPI_COMM_WORLD);
    return global;
}

// Function to compute the error for a given process (with input u = uNum - uExa)
double norme_L2(SpMatrix& M, ScaVector& u, Mesh& mesh, ScaVector& maskForResidual){
  ScaVector Mu = M*u;
  exchangeAddInterfMPI(Mu, mesh);
  double norm = sqrt(prod_scal1(u, Mu, maskForResidual));
  return norm;
}

double u(double x, double y)
{
  return cos(4*M_PI*x)*cos(M_PI*y);
}

int main(int argc, char* argv[])
{
  
  // 1. Initialize MPI
  MPI_Init(&argc, &argv);
  MPI_Comm_rank(MPI_COMM_WORLD, &myRank);
  MPI_Comm_size(MPI_COMM_WORLD, &nbTasks);

  // 2. Read the mesh, and build lists of nodes for MPI exchanges, local numbering
  Mesh mesh;
  readMsh(mesh, "benchmark/mesh.msh");
  buildListsNodesMPI(mesh);
  
  // 3. Build problem (vectors and matrices)
  ScaVector uNum(mesh.nbOfNodes);
  ScaVector uExa(mesh.nbOfNodes);
  ScaVector f(mesh.nbOfNodes);
  for(int i=0; i<mesh.nbOfNodes; ++i){
    double x = mesh.coords(i,0);
    double y = mesh.coords(i,1);
    uNum(i) = 0.;
    uExa(i) = u(x, y);
    f(i) = (1. + 17*M_PI*M_PI) * u(x, y);
  }
  
  Problem pbm;
  double alpha = 1;
  buildProblem(pbm,mesh,alpha,f);
  
  // 4. Solve problem
  double tol = 1e-8;
  int maxit = 50000;
  // jacobi(pbm.A, pbm.b, uNum, mesh, tol, maxit);
  gradient_conjugue(pbm.A, pbm.b, uNum, mesh, tol, maxit);
  
  // 5. Compute error and export fields
  ScaVector uErr = uNum - uExa;
  double err_2 = norme_L2(pbm.M, uErr, mesh, maskForResidual);
  cout << "   -> error (L^2 norm) = " << err_2 << endl;
  exportFieldMsh(uNum, mesh, "solNum", "benchmark/solNum.msh");
  exportFieldMsh(uExa, mesh, "solRef", "benchmark/solExa.msh");
  exportFieldMsh(uErr, mesh, "solErr", "benchmark/solErr.msh");

  // 6. Finilize MPI
  MPI_Barrier(MPI_COMM_WORLD);
  MPI_Finalize();
  
  return 0;
}