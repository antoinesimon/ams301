#include "headers.hpp"

extern int myRank;
extern int nbTasks;

extern ScaVector maskForResidual;

//================================================================================
// Solution of the system Au=b with Jacobi
//================================================================================

double prod_scal(ScaVector& u, ScaVector& v, ScaVector& maskForResidual){
    double local = 0;
    double global = 0;

    for(int i =0; i < u.size(); i++){
      local += u(i)*v(i)*maskForResidual(i);
    }

    MPI_Allreduce(&local, &global, 1, MPI_DOUBLE, MPI_SUM, MPI_COMM_WORLD);
    return global;
}

double residu_global(SpMatrix& A, ScaVector& b, ScaVector& u, Mesh& mesh, ScaVector& maskForResidual){
  ScaVector Au = A*u;
  exchangeAddInterfMPI(Au, mesh);
  ScaVector Vres = Au - b;
  double res = sqrt(prod_scal(Vres, Vres, maskForResidual));
  return res;
}

void jacobi(SpMatrix& A, ScaVector& b, ScaVector& u, Mesh& mesh, double tol, int maxit)
{
  if(myRank == 0)
    cout << "== jacobi" << endl;
  
  // Compute the solver matrices
  int size = A.rows();
  ScaVector Mdiag(size);
  SpMatrix N(size, size);
  for(int k=0; k<A.outerSize(); ++k){
    for(SpMatrix::InnerIterator it(A,k); it; ++it){
      if(it.row() == it.col())
        Mdiag(it.row()) = it.value();
      else
        N.coeffRef(it.row(), it.col()) = -it.value();
    }
  }
  exchangeAddInterfMPI(Mdiag, mesh);
  
  // Jacobi solver
  double residuNorm = 1e2;
  int it = 0;

  double timeBegin = MPI_Wtime();

  while (residuNorm > tol && it < maxit){
    
    // Compute N*u
    ScaVector Nu = N*u;
    exchangeAddInterfMPI(Nu, mesh);
    
    // Update field
    for(int i=0; i<size; i++){
      u(i) = 1/Mdiag(i) * (Nu(i) + b(i));
    }
    
    // Update residual and iterator
    residuNorm = residu_global(A, b, u, mesh, maskForResidual);
    
    if((it % 100) == 0){
      if(myRank == 0)
        cout << "   [" << it << "] residual: " << residuNorm << endl;
    }
    it++;
  }

  MPI_Barrier(MPI_COMM_WORLD);
  if (myRank == 0) {
    double timeEnd = MPI_Wtime();
    double runtime = timeEnd - timeBegin;
    cout << "   -> total runtime = " << runtime << " s" << endl;
  }
  
  if(myRank == 0){
    cout << "   -> final iteration: " << it << " (prescribed max: " << maxit << ")" << endl;
    cout << "   -> final residual: " << residuNorm << " (prescribed tol: " << tol << ")" << endl;
  }
}

//================================================================================
// Solution of the system Au=b with Conjugate Gradient
//================================================================================

void gradient_conjugue(SpMatrix& A, ScaVector& b, ScaVector& u, Mesh& mesh, double tol, int maxit)
{
  if(myRank == 0)
    cout << "== Gradient conjugué" << endl;
  
  // Compute the solver matrices
  int size = A.rows();
  ScaVector Mdiag(size);
  SpMatrix N(size, size);
  for(int k=0; k<A.outerSize(); ++k){
    for(SpMatrix::InnerIterator it(A,k); it; ++it){
      if(it.row() == it.col())
        Mdiag(it.row()) = it.value();
      else
        N.coeffRef(it.row(), it.col()) = -it.value();
    }
  }
  exchangeAddInterfMPI(Mdiag, mesh);
  
  // Conjugate gradient solver
  double residuNorm = 1e2;
  double epsilon = 1;
  int it = 0;
  ScaVector r = b-A*u;
  double res0 = sqrt(prod_scal(r, r, maskForResidual));
  ScaVector p = r;
  double alpha = 0;
  double beta = 0;
  double truc = 0;
  
  double timeBegin = MPI_Wtime();

  while (residuNorm > tol && it < maxit){
    ScaVector Ap = A*p;
    exchangeAddInterfMPI(Ap, mesh);
    alpha = prod_scal(r, p, maskForResidual) / prod_scal(p, Ap, maskForResidual);
    u = u + alpha*p;
    r = r - alpha*Ap;
    beta = - prod_scal(r, Ap, maskForResidual)/ prod_scal(p, Ap, maskForResidual);
    p= r + beta*p;

    // Update residual and iterator
    residuNorm = sqrt(prod_scal(r, r, maskForResidual));
    
    if((it % 100) == 0){
      if(myRank == 0)
        cout << "   [" << it << "] residual: " << residuNorm << endl;
    }
    it++;
  }

  MPI_Barrier(MPI_COMM_WORLD);
  if (myRank == 0) {
    double timeEnd = MPI_Wtime();
    double runtime = timeEnd - timeBegin;
    cout << "   -> total runtime = " << runtime << " s" << endl;
  }
  
  if(myRank == 0){
    cout << "   -> final iteration: " << it << " (prescribed max: " << maxit << ")" << endl;
    cout << "   -> final residual: " << residuNorm << " (prescribed tol: " << tol << ")" << endl;
  }
}
