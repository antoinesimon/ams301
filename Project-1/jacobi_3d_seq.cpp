// Compilation:
//   g++ jacobi_3d_seq.cpp
// Execution (replace 'Nx', 'Ny', 'Nz' and 'L' with numbers of spatial/time steps):
//   ./a.out 'Nx' 'Ny' 'Nz' 'L'

#include <iostream>
#include <fstream>
#include <cmath>
#include <vector>
#include <chrono>

using namespace std;

double u_manu(double x, double y, double z)
{
  return sin(4*M_PI*x*y*z);
}

// Then to use these manufactured solutions we have u_0 that allows to build the initial solution with boundary conditions and 0 everywhere else
double u_0(int nx, double dx, int Nx, int ny, double dy, int Ny, int nz, double dz, int Nz)
{
  if (nx == 0 || nx == Nx+1 || ny == 0 || ny == Ny+1 || nz == 0 || nz == Nz+1) {
    // return u_manu_1(nx*dx, ny*dy);
    return u_manu(nx*dx, ny*dy, nz*dz);
  } else {
    return 0.;
  }
}

double f_manu(double x, double y, double z)
{
  return -16*M_PI*M_PI * (y*y*z*z + x*x*(y*y + z*z)) * u_manu(x, y, z);
}

// Finally we initialize the necessary functions to simulate the equation asked by the subject
// We solve for f = 0
double f(double x, double y, double z)
{
  return f_manu(x, y, z);
  // return 0.;
}

int main(int argc, char* argv[]){

  // Problem parameters
  if (argc!=5){
    cout << "You need to input 4 variables: Nx, Ny, Nz and L, here there are " << argc-1 << " variables" << endl;
    return 1;
  }
  int Nx = atoi(argv[1]);
  int Ny = atoi(argv[2]);
  int Nz = atoi(argv[3]);
  int L = atoi(argv[4]); // Maximum number of iterations

  int U_zero = 0.;
  double epsilon_tol = 1e-5;
  double dx = 1./(Nx+1.);
  double dy = 1./(Ny+1.);
  double dz = 1./(Nz+1.);
  double coef = 2*(1/(dx*dx) + 1/(dy*dy) + 1/(dz*dz));

  // Memory allocation + Initial solution + Boundary conditions + Residual
  vector<vector<vector<double>>> sol(Nx+2, vector<vector<double>>(Ny+2, vector<double>(Nz+2, U_zero)));
  vector<vector<vector<double>>> solNew(Nx+2, vector<vector<double>>(Ny+2, vector<double>(Nz+2, U_zero)));
  // The residuals will represent the residual *squared* because we use the euclidian norm but we will make comparisons with the square root of these values
  double residual = 0.;
  double residual_0 = 0.;
  // In order to lighten the code we use a temporary value
  double temp_residual_i_j_k;

  // Initialization of the bounadry conditions * for manufactured solutions *
  for (int i=0; i<=Nx+1; i++){
    for (int j=0; j<=Ny+1; j++){
      for (int k=0; k<=Nz+1; k++){
        sol[i][j][k] = u_0(i, dx, Nx, j, dy, Ny, k, dz, Nz);
        solNew[i][j][k] = u_0(i, dx, Nx, j, dy, Ny, k, dz, Nz);
      }
    }
  }

  // Computation of the first residual residual_0
  for (int i=1; i<=Nx; i++){
    for (int j=1; j<=Ny; j++){
      for (int k=1; k<=Nz; k++){
        temp_residual_i_j_k = f(i*dx, j*dy, k*dz) - (-coef*sol[i][j][k] + (1/(dx*dx)*(sol[i+1][j][k] + sol[i-1][j][k]) + 1/(dy*dy)*(sol[i][j+1][k] + sol[i][j-1][k]) + 1/(dz*dz)*(sol[i][j][k+1] + sol[i][j][k-1])));
        residual_0 += temp_residual_i_j_k*temp_residual_i_j_k;
      }
    }
  }
  residual = residual_0;

  // We compute the time it takes to the program to reach a solution
  using std::chrono::high_resolution_clock;
  using std::chrono::duration_cast;
  using std::chrono::milliseconds;

  auto t_begin = high_resolution_clock::now();

  // Initialization of a temporary variable to avoid computing the same operations multiple times
  double temp;
  // Initialization of the time loop
  int l = 1;
  while (l<=L && sqrt(residual/residual_0) > epsilon_tol){
    residual = 0.;
    // Spatial loops
    for (int i=1; i<=Nx; i++){
      for (int j=1; j<=Ny; j++){
        for (int k=1; k<=Nz; k++){
          temp = (1/(dx*dx)*(sol[i+1][j][k] + sol[i-1][j][k]) + 1/(dy*dy)*(sol[i][j+1][k] + sol[i][j-1][k]) + 1/(dz*dz)*(sol[i][j][k+1] + sol[i][j][k-1]));

          solNew[i][j][k] = 1/coef*(temp - f(i*dx,j*dy, k*dz));

          temp_residual_i_j_k = f(i*dx,j*dy, k*dz) - (-coef*sol[i][j][k] + temp);
          residual += temp_residual_i_j_k*temp_residual_i_j_k;
        }
      }
    }
    // Swap pointers
    sol.swap(solNew);
    // Increment l
    l++;
  }

  auto t_end = high_resolution_clock::now();
  // Getting number of milliseconds as an integer
  auto ms_int = duration_cast<milliseconds>(t_end - t_begin);
  cout << "Duration : " << ms_int.count() << " (ms)" << endl;

  // Print the residual and rank at which it stopped
  cout << "Residual/Residual_0 = " << sqrt(residual/residual_0) << endl;
  cout << "Stopped at time l = " << l << endl;

  // Print the error between the theorical solution and the simulation
  double error =0.;
  for (int i=0; i<=Nx+1; i++){
    for (int j=0; j<=Ny+1; j++){
      for (int k=0; k<=Nz+1; k++){
        error += (u_manu(i*dx, j*dy, k*dz) - sol[i][j][k]) * (u_manu(i*dx, j*dy, k*dz) - sol[i][j][k]);
      }
    }
  }
  cout << "Error (2-norm normalized by Nx.Ny.Nz) = " << error/Nx/Ny/Nz << endl;

  return 0;
}
