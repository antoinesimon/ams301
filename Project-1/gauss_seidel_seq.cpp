// Compilation:
//   g++ gauss_seidel_seq.cpp
// Execution (replace 'Nx', 'Ny', 'L' and 'alpha' with numbers of spatial/time steps):
//   ./a.out 'Nx' 'Ny' 'L' 'alpha'

#include <iostream>
#include <fstream>
#include <cmath>
#include <vector>
#include <chrono>

using namespace std;

// We build 2 manufactured solutions u_manu_1 and u_manu_2 that will only be used to validate the simulator
double u_manu_1(double x, double y)
{
  return cos(M_PI*x)*cos(M_PI*y);
}

double u_manu_2(double x, double y)
{
  return sin(4*M_PI*x*y);
}

// Then to use these manufactured solutions we have u_0 that allows to build the initial solution with boundary conditions and 0 everywhere else
double u_0(int nx, double dx, int Nx, int ny, double dy, int Ny)
{
  if (nx == 0 || nx == Nx+1 || ny == 0 || ny == Ny+1) {
    // return u_manu_1(nx*dx, ny*dy);
    return u_manu_2(nx*dx, ny*dy);
  } else {
    return 0.;
  }
}

// The Laplacians associated the manufactured solutions
double f_manu_1(double x, double y)
{
  return -2.*M_PI*M_PI*u_manu_1(x,y);
}

double f_manu_2(double x, double y)
{
  return -16*M_PI*M_PI * (x*x+y*y) * u_manu_2(x, y);
}

// Finally we initialize the necessary functions to simulate the equation asked by the subject
// We solve for f = 0
double f(double x, double y)
{
  // return f_manu_1(x, y);
  // return f_manu_2(x, y);
  return 0.;
}

int main(int argc, char* argv[]){

  // Problem parameters
  if (argc!=5) {
    cout << "You need to input 4 variables: Nx, Ny, L and alpha, here there are " << argc-1 << " variables" << endl;
    return 1;
  }
  int Nx = atoi(argv[1]);
  int Ny = atoi(argv[2]);
  int L = atoi(argv[3]); // Maximum number of iterations
  int alpha = atoi(argv[4]);

  int U_zero = 1.;
  double epsilon_tol = 1e-4;
  double dx = 1./(Nx+1.);
  double dy = 1./(Ny+1.);
  double coef = 2*(1/(dx*dx) + 1/(dy*dy));

  // Memory allocation + Initial solution + Boundary conditions + Residual
  vector<vector<double>> sol(Nx+2, vector<double>(Ny+2, U_zero));
  vector<vector<double>> solNew(Nx+2, vector<double>(Ny+2, U_zero));
  // The residuals will represent the residual *squared* because we use the euclidian norm but we will make comparisons with the sqaure root of these values
  vector<vector<double>> residual_vec(Nx, vector<double>(Ny, 0.));
  double residual = 0.;
  double residual_0 = 0.;

  // Initialization of the bounadry conditions * for manufactured solutions *
  /*for (int i=0; i<=Nx+1; i++){
    for (int j=0; j<=Ny+1; j++){
      sol[i][j] = u_0(i, dx, Nx, j, dy, Ny);
      solNew[i][j] = u_0(i, dx, Nx, j, dy, Ny);
    }
  }*/

  // Initialization for the resolution of the equation from the subject
  for (int j=0; j<=Ny+1; j++){
    sol[0][j] = U_zero*(1+alpha*(1 - cos(2*M_PI*j/Ny)));
    solNew[0][j] = U_zero*(1+alpha*(1 - cos(2*M_PI*j/Ny)));
    sol[Nx+1][j] = U_zero;
    solNew[Nx+1][j] = U_zero;
  }
  for (int i=0; i<=Nx+1; i++){
    sol[i][0] = U_zero;
    solNew[i][0] = U_zero;
    sol[i][Ny+1] = U_zero;
    solNew[i][Ny+1] = U_zero;
  }

  // Computation of the first residual residual_0
  for (int i=1; i<=Nx; i++){
    for (int j=1; j<=Nx; j++){
      residual_vec[i-1][j-1] = f(i*dx, j*dy) - (-coef*sol[i][j] + (1/(dx*dx)*(sol[i+1][j] + sol[i-1][j]) + 1/(dy*dy)*(sol[i][j+1] + sol[i][j-1])));
      residual_0 += residual_vec[i-1][j-1]*residual_vec[i-1][j-1];
    }
  }
  residual = residual_0;

  using std::chrono::high_resolution_clock;
  using std::chrono::duration_cast;
  using std::chrono::milliseconds;

  auto t1 = high_resolution_clock::now();

  // Initialization of a temporary variable to avoid computing the same operations multiple times
  double temp;
  // Initialization of the time loop
  int l = 0;
  while (l<=L && sqrt(residual/residual_0) > epsilon_tol) {
    residual = 0.;
    // Spatial loops
    for (int i=1; i<=Nx; i++) {
      for (int j=1; j<=Ny; j++){
        solNew[i][j] = 1/coef*(1/(dx*dx)*(sol[i+1][j] + solNew[i-1][j]) + 1/(dy*dy)*(sol[i][j+1] + solNew[i][j-1]) - f(i*dx,j*dy));
        
        temp = f(i*dx,j*dy) - (-coef*sol[i][j] + (1/(dx*dx)*(sol[i+1][j] + sol[i-1][j]) + 1/(dy*dy)*(sol[i][j+1] + sol[i][j-1])));
        residual_vec[i-1][j-1] = temp*temp;
        residual += residual_vec[i-1][j-1];
      }
    }
    // Swap pointers
    sol.swap(solNew);
    // Increment l
    l++;
  }

  auto t2 = high_resolution_clock::now();
  // Getting number of milliseconds as an integer
  auto ms_int = duration_cast<milliseconds>(t2 - t1);
  cout << "Duration : " << ms_int.count() << " (ms)" << endl;

  // Print the residual and rank at which it stopped
  cout << "Residual/Residual_0 = " << sqrt(residual/residual_0) << endl;
  cout << "Stopped at time l = " << l << endl;

  // Print solution
  ofstream file;
  file.open("gauss_seidel.dat");
  for (int i=0; i<=Nx+1; i++) {
    for (int j=0; j<=Ny+1; j++) {
      file << i*dx << " " << j*dy << " " << solNew[i][j] << endl;
    }
    file << endl;
  }
  file.close();

  return 0;
}
