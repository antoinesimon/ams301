// Compilation:
//   mpicxx jacobi_para.cpp
// Execution on P process (replace 'Nx', 'Ny', 'L' and 'alpha' with numbers of spatial/time steps):
//   mpirun -np P ./a.out 'Nx' 'Ny' 'L' 'alpha'

#include <iostream>
#include <mpi.h>
#include <fstream>
#include <cmath>
#include <vector>

using namespace std;

double u(double x, double y)
{
  //return cos(M_PI*x)*cos(M_PI*y);
  return 0;
}

double f(double x, double y)
{
  //return -2.*M_PI*M_PI*u(x,y);
  return 0;
}

int main(int argc, char* argv[]){
  
  MPI_Init(&argc, &argv);

  int nbTask;
  int myRank;
  MPI_Comm_size(MPI_COMM_WORLD, &nbTask);
  MPI_Comm_rank(MPI_COMM_WORLD, &myRank);

  // Problem parameters
  if (argc!=5){
    cout << "You need to input 4 variables: Nx, Ny, L and alpha, here there are " << argc-1 << " variables" << endl;
    return 1;
  }
  int Nx = atoi(argv[1]);
  int Ny = atoi(argv[2]);
  int L = atoi(argv[3]); // Nombre max d'iter
  int alpha = atoi(argv[4]);

  // The residuals will represent the residual *squared* because we use the euclidian norm but we will make comparisons with the sqaure root of these values
  double residual = 0.;
  double residual_0 = 0.;
  double temp_res = 0.;
  
  // Initialisation of parameters
  int U_zero = 1;
  double temp ;
  double epsilon_tol = 1e-4;
  double dx = 1./(Nx+1.);
  double dy = 1./(Ny+1.);
  double coef = 2*(1/(dx*dx) + 1/(dy*dy));
  
  int i_start = myRank * ((Nx + 1)/nbTask)+1;
  int i_end = ((myRank+1) * ((Nx + 1)/nbTask));
  i_end = (i_end < Nx+1) ? i_end : Nx+1;
  int x_len = i_end + 1 - i_start;

  cout << "i start : " << i_start << ", i end : " << i_end << endl;

  MPI_Request req;

  // Each process is working on a part of the original mesh
  vector<vector<double>> sol(x_len, vector<double>(Ny+2, U_zero));
  vector<vector<double>> solNew(x_len, vector<double>(Ny+2, U_zero));
  
  // Depending on the placement of the process, a vector is created to get the border values of the lower and/or upper process
  vector<double> lower_row(Ny+2);
  vector<double> upper_row(Ny+2);

  //CI 
  if(myRank == 0){
    for (int j=0; j<=Ny+1; j++){
      sol[0][j] = U_zero*(1+alpha*(1 - cos(2*M_PI*j/Ny)));
      solNew[0][j] = U_zero*(1+alpha*(1 - cos(2*M_PI*j/Ny)));
      //sol[0][j] = u(0, dy*j);
      //solNew[0][j] = u(0, dy*j);
    }
  }


  // Computation of the first residual residual_0
  for (int i=1; i<=x_len - 2; i++){
    for (int j=1; j<=Ny; j++){
      temp = f(i*dx, j*dy) - (-coef*sol[i][j] + (1/(dx*dx)*(sol[i+1][j] + sol[i-1][j]) + 1/(dy*dy)*(sol[i][j+1] + sol[i][j-1])));
      temp_res += temp*temp;
    }
  }

  MPI_Allreduce(&temp_res, &residual_0, 1, MPI_DOUBLE, MPI_SUM, MPI_COMM_WORLD);
  residual = residual_0;


  // Initialization of the time loop
  int l = 0;
  
  // Check time
  double timeInit = MPI_Wtime();

  // Temporal loop
  while(l < L && sqrt(residual/residual_0) > epsilon_tol){
    temp_res = 0;
    //Each process sends and receives the border row regarding its placement
    if (myRank != 0){
      MPI_Isend(&sol[0][0] , Ny+2 , MPI_DOUBLE , myRank-1 , 66 , MPI_COMM_WORLD , &req);
    }

    if (myRank != nbTask-1){
      MPI_Isend(&sol[x_len - 1][0] , Ny+2 , MPI_DOUBLE , myRank+1 , 65 , MPI_COMM_WORLD , &req);
    }

    // The border values are now computed with the received rows
    if (myRank != 0){
      MPI_Recv(&lower_row[0] , Ny+2 , MPI_DOUBLE , myRank-1 , 65 , MPI_COMM_WORLD , MPI_STATUS_IGNORE );
      
      //case i = 0
      for (int j=1; j<=Ny; j++){
        //solNew[0][j] = 1/coef*(1/(dx*dx)*(sol[1][j] + lower_row[j]) + 1/(dy*dy)*(sol[0][j+1] + sol[0][j-1])- f(0*dx,j*dy));
        
        temp = (1/(dx*dx)*(sol[1][j] + lower_row[j]) + 1/(dy*dy)*(sol[0][j+1] + sol[0][j-1]));
        solNew[0][j] = 1/coef*(temp - f(0*dx,j*dy));

        temp = f(0*dx,j*dy) - (-coef*sol[0][j] + temp);
        temp_res += temp*temp;
      }
    }
    
    //case i = i_end
    if (myRank != nbTask-1){
      MPI_Recv(&upper_row[0] , Ny+2 , MPI_DOUBLE , myRank+1 , 66 , MPI_COMM_WORLD , MPI_STATUS_IGNORE );

      for (int j=1; j<=Ny; j++){
        //solNew[x_len - 1][j] = 1/coef*(1/(dx*dx)*(upper_row[j] + sol[x_len - 2][j]) + 1/(dy*dy)*(sol[x_len - 1][j+1] + sol[x_len - 1][j-1])- f((i_end)*dx,j*dy));
        
        temp = (1/(dx*dx)*(upper_row[j] + sol[x_len - 2][j]) + 1/(dy*dy)*(sol[x_len - 1][j+1] + sol[x_len - 1][j-1]));

        solNew[x_len - 1][j] = 1/coef*(temp - f((i_end)*dx,j*dy));

        temp = f((i_end)*dx,j*dy) - (-coef*sol[x_len-1][j] + temp);
        temp_res +=  temp*temp;
      }
    }

    // Spatial loop
    for (int i=1; i<=x_len - 2; i++){
      // Spatial loop
      for (int j=1; j<=Ny; j++){
        // solNew[i][j] = 1/coef*(1/(dx*dx)*(sol[i+1][j] + sol[i-1][j]) + 1/(dy*dy)*(sol[i][j+1] + sol[i][j-1]) - f((i + i_start)*dx,j*dy));
        temp = (1/(dx*dx)*(sol[i+1][j] + sol[i-1][j]) + 1/(dy*dy)*(sol[i][j+1] + sol[i][j-1]));

        solNew[i][j] = 1/coef*(temp - f((i + i_start)*dx,j*dy));

        temp = f((i + i_start)*dx,j*dy) - (-coef*sol[i][j] + temp);
        temp_res +=  temp*temp;
      }
    }
    MPI_Allreduce(&temp_res, &residual, 1, MPI_DOUBLE, MPI_SUM, MPI_COMM_WORLD);
    // Swap pointers
    sol.swap(solNew);
    
    // Increment l
    l++;
  }

  // Check time
  MPI_Barrier(MPI_COMM_WORLD);
  if(myRank == 0){
    double timeEnd = MPI_Wtime();
    cout << "Runtime: " << timeEnd-timeInit << endl;
  }

  cout << "n iter = " << l << "; res/res_0 = " <<sqrt(residual/residual_0) <<endl;
  
  // Print solution (single file)
  for (int myRankPrint=0; myRankPrint<nbTask; myRankPrint++){
    if (myRank == myRankPrint){
      ofstream file;
      if(myRank == 0){
        file.open("jacobi_mpi.dat", ios::out);
      } else {
        file.open("jacobi_mpi.dat", ios::app);
      }
      for (int i=i_start; i<=i_end; i++){
        for (int j=0; j<=Ny+1; j++){
          file << i*dx << " " << j*dy << " " << sol[i - i_start][j] << endl;
        }
        file << endl;
      }
      file.close();
    }
    MPI_Barrier(MPI_COMM_WORLD);
  }

  // Finalize MPI
  MPI_Barrier(MPI_COMM_WORLD);
  MPI_Finalize();
  return 0;
}
