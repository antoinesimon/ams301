// Compilation:
//   mpicxx jacobi_3d_para.cpp
// Execution on P process (replace 'Nx', 'Ny', 'Nz' and 'L' with numbers of spatial/time steps):
//   mpirun -np P ./a.out 'Nx' 'Ny' 'Nz' 'L'

#include <iostream>
#include <mpi.h>
#include <fstream>
#include <cmath>
#include <vector>

using namespace std;

double u_manu(double x, double y, double z)
{
  return sin(4*M_PI*x*y*z);
}

// Then to use these manufactured solutions we have u_0 that allows to build the initial solution with boundary conditions and 0 everywhere else
double u_0(int nx, double dx, int Nx, int ny, double dy, int Ny, int nz, double dz, int Nz)
{
  if (nx == 0 || nx == Nx+1 || ny == 0 || ny == Ny+1 || nz == 0 || nz == Nz+1) {
    // return u_manu_1(nx*dx, ny*dy);
    return u_manu(nx*dx, ny*dy, nz*dz);
  } else {
    return 0.;
  }
}

double f_manu(double x, double y, double z)
{
  return -16*M_PI*M_PI * (y*y*z*z + x*x*(y*y + z*z)) * u_manu(x, y, z);
}

// Finally we initialize the necessary functions to simulate the equation asked by the subject
// We solve for f = 0
double f(double x, double y, double z)
{
  return f_manu(x, y, z);
  // return 0.;
}

int main(int argc, char* argv[]){

  MPI_Init(&argc, &argv);

  int nbTask;
  int myRank;
  MPI_Comm_size(MPI_COMM_WORLD, &nbTask);
  MPI_Comm_rank(MPI_COMM_WORLD, &myRank);

  // Problem parameters
  if (argc!=5){
    cout << "You need to input 4 variables: Nx, Ny, Nz and L, here there are " << argc-1 << " variables" << endl;
    return 1;
  }
  int Nx = atoi(argv[1]);
  int Ny = atoi(argv[2]);
  int Nz = atoi(argv[3]);
  int L = atoi(argv[4]); // Maximum number of iterations

  int U_zero = 0.;
  double epsilon_tol = 1e-3;
  double dx = 1./(Nx+1.);
  double dy = 1./(Ny+1.);
  double dz = 1./(Nz+1.);
  double coef = 2*(1/(dx*dx) + 1/(dy*dy) + 1/(dz*dz));

  int i_start = myRank * ((Nx+1)/nbTask)+1;
  int i_end = ((myRank+1) * ((Nx+1)/nbTask));
  i_end = (i_end <= Nx+1) ? i_end : Nx+1;
  int i_size = i_end - i_start + 1;

  MPI_Request req;

  // Memory allocation + Initial solution + Boundary conditions + Residual
  vector<vector<vector<double>>> sol(i_size, vector<vector<double>>(Ny+2, vector<double>(Nz+2, U_zero)));
  vector<vector<vector<double>>> solNew(i_size, vector<vector<double>>(Ny+2, vector<double>(Nz+2, U_zero)));
  // The residuals will represent the residual *squared* because we use the euclidian norm but we will make comparisons with the square root of these values
  double residual_total = 0.;
  double residual_0_total = 0.;
  // In order to lighten the code we use a temporary value
  double temp_residual_i_j_k = 0.;
  double partial_residual = 0.;

  // We create vectors that will store in one line the planes we need to communicate between processes
  vector<vector<double>> upper_plane((Ny+2), vector<double>(Nz+2, 0.));
  vector<vector<double>> lower_plane((Ny+2), vector<double>(Nz+2, 0.));

  // Initialization of the bounadry conditions * for manufactured solutions *
  for (int i=0; i<i_size; i++){
    for (int j=0; j<=Ny+1; j++){
      for (int k=0; k<=Nz+1; k++){
        sol[i][j][k] = u_0(i+i_start, dx, Nx, j, dy, Ny, k, dz, Nz);
        solNew[i][j][k] = u_0(i+i_start, dx, Nx, j, dy, Ny, k, dz, Nz);
      }
    }
  }

  // Computation of the first partial residual residual_0
  for (int i=1; i<i_size-1; i++){
    for (int j=1; j<=Ny; j++){
      for (int k=1; k<=Nz; k++){
        temp_residual_i_j_k = f((i+i_start)*dx, j*dy, k*dz) - (-coef*sol[i][j][k] + (1/(dx*dx)*(sol[i+1][j][k] + sol[i-1][j][k]) + 1/(dy*dy)*(sol[i][j+1][k] + sol[i][j-1][k]) + 1/(dz*dz)*(sol[i][j][k+1] + sol[i][j][k-1])));
        partial_residual += temp_residual_i_j_k*temp_residual_i_j_k;
      }
    }
  }

  MPI_Allreduce(&partial_residual, &residual_0_total, 1, MPI_DOUBLE, MPI_SUM, MPI_COMM_WORLD);
  residual_total = residual_0_total;

  // Initialization of a temporary variable to avoid computing the same operations multiple times
  double temp;
  // Initialization of the time loop
  int l = 1;

  // Check time
  double timeInit = MPI_Wtime();

  while (l<=L && sqrt(residual_total/residual_0_total) > epsilon_tol){
    partial_residual = 0.;

    // If we're not process 0 then we need to send the lower plane to the previous process
    if (myRank != 0){
      for (int jj=0; jj<=Ny+1; jj++){
        MPI_Isend(&sol[0][jj][0] , Nz+2 , MPI_DOUBLE , myRank-1 , jj , MPI_COMM_WORLD , &req);
      }
    }

    // If we're not process nbTask-1 then we need to send the upper plane to the next process
    if (myRank != nbTask-1){
      for (int jj=0; jj<=Ny+1; jj++){
        MPI_Isend(&sol[i_size-1][jj][0] , Nz+2 , MPI_DOUBLE , myRank+1 , Ny+2+jj , MPI_COMM_WORLD , &req);
      }
    }

    // The border values are now computed with the received planes
    if (myRank != 0){
      for (int jj=0; jj<=Ny+1; jj++){
        MPI_Recv(&lower_plane[jj][0] , Nz+2 , MPI_DOUBLE , myRank-1 , Ny+2+jj , MPI_COMM_WORLD , MPI_STATUS_IGNORE);
      }

      //case i = i_start
      for (int jj=1; jj<=Ny; jj++){
        for (int kk=1; kk<=Nz; kk++){
          temp = (1/(dx*dx)*(sol[1][jj][kk] + lower_plane[jj][kk]) + 1/(dy*dy)*(sol[0][jj+1][kk] + sol[0][jj-1][kk]) + 1/(dz*dz)*(sol[0][jj][kk+1] + sol[0][jj][kk-1]));
          solNew[0][jj][kk] = 1/coef*(temp - f(i_start*dx, jj*dy, kk*dz));

          temp_residual_i_j_k = f(i_start*dx, jj*dy, kk*dz) - (-coef*sol[0][jj][kk] + temp);
          partial_residual += temp_residual_i_j_k*temp_residual_i_j_k;
        }
      }
    }

    if (myRank != nbTask-1){
      for (int jj=0; jj<=Ny+1; jj++){
        MPI_Recv(&upper_plane[jj][0] , Nz+2 , MPI_DOUBLE , myRank+1 , jj , MPI_COMM_WORLD , MPI_STATUS_IGNORE);
      }

      //case i = i_end
      for (int jj=1; jj<=Ny; jj++){
        for (int kk=1; kk<=Nz; kk++){
          temp = (1/(dx*dx)*(upper_plane[jj][kk] + sol[i_size-2][jj][kk]) + 1/(dy*dy)*(sol[i_size-1][jj+1][kk] + sol[i_size-1][jj-1][kk]) + 1/(dz*dz)*(sol[i_size-1][jj][kk+1] + sol[i_size-1][jj][kk-1]));

          solNew[i_size-1][jj][kk] = 1/coef*(temp - f(i_end*dx, jj*dy, kk*dz));

          temp_residual_i_j_k = f(i_end*dx, jj*dy, kk*dz) - (-coef*sol[i_size-1][jj][kk] + temp);
          partial_residual +=  temp_residual_i_j_k*temp_residual_i_j_k;
        }
      }
    }

    // Spatial loops
    for (int i=1; i<=i_size-2; i++){
      for (int j=1; j<=Ny; j++){
        for (int k=1; k<=Nz; k++){
          temp = (1/(dx*dx)*(sol[i+1][j][k] + sol[i-1][j][k]) + 1/(dy*dy)*(sol[i][j+1][k] + sol[i][j-1][k]) + 1/(dz*dz)*(sol[i][j][k+1] + sol[i][j][k-1]));

          solNew[i][j][k] = 1/coef*(temp - f((i_start+i)*dx, j*dy, k*dz));

          temp_residual_i_j_k = f((i_start+i)*dx, j*dy, k*dz) - (-coef*sol[i][j][k] + temp);
          partial_residual += temp_residual_i_j_k*temp_residual_i_j_k;
        }
      }
    }

    MPI_Allreduce(&partial_residual, &residual_total, 1, MPI_DOUBLE, MPI_SUM, MPI_COMM_WORLD);

    // Swap pointers
    sol.swap(solNew);
    // Increment l
    l++;
  }

  // Check time
  MPI_Barrier(MPI_COMM_WORLD);
  if(myRank == 0){
    double timeEnd = MPI_Wtime();
    cout << "Runtime : " << timeEnd-timeInit << " s" << endl;

    // Print the residual and rank at which it stopped
    cout << "Residual/Residual_0 = " << sqrt(residual_total/residual_0_total) << endl;
    cout << "Stopped at time l = " << l << endl;
  }

  // Print the error between the theorical solution and the simulation
  double error = 0.;
  double partial_error = 0.;
  for (int i=0; i<=i_size-1; i++){
    for (int j=0; j<=Ny+1; j++){
      for (int k=0; k<=Nz+1; k++){
        partial_error += (u_manu((i_start+i)*dx, j*dy, k*dz) - sol[i][j][k]) * (u_manu((i_start+i)*dx, j*dy, k*dz) - sol[i][j][k]);
      }
    }
  }
  MPI_Reduce(&partial_error, &error, 1, MPI_DOUBLE, MPI_SUM, 0, MPI_COMM_WORLD);
  if (myRank == 0) {
    cout << "Error (2-norm normalized by Nx.Ny.Nz) = " << sqrt(error)/Nx/Ny/Nz << endl;
  }

  // Finalize MPI
  MPI_Barrier(MPI_COMM_WORLD);
  MPI_Finalize();
  return 0;
}
